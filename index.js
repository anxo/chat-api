const fs = require('fs')
const express = require('express')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const app = express()
const port = process.env.PORT || 5000
const secret = process.env.SECRET || 'change-me'

const chats = {}
const initialRooms = ['general', 'clase', 'random', 'vip.general']
const maxMessages = 25
initialRooms.forEach(r => chats[r] = [])

app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
  res.contentType('html')
  res.send('<pre>' + fs.readFileSync('./README.md') + '</pre>')
})

app.get('/rooms', (req, res) => {
  let rooms = Object.keys(chats)
  const token = req.headers.authorization && req.headers.authorization.substr(7)
  let me
  if (token) {
    try {
      me = jwt.verify(token, secret)
    } catch (e) {
      res.status(403).send({ error: "Bad token" })
      return
    }
  }
  if (!me) rooms = rooms.filter(r => !r.startsWith("vip"))
  res.send(rooms)
})

app.get('/rooms/:room', (req, res) => {
  const room = req.params.room
  if (chats[room]) {
    if (room.startsWith("vip")) {
      try {
        const token = req.headers.authorization && req.headers.authorization.substr(7)
        const me = token && jwt.verify(token, secret)
        if (!me) {
          res.status(401).send({ error: "Channel requires auth" })
          return
        }
      } catch (e) {
        res.status(403).send({ error: "Bad token" })
        return
      }
    }
    res.send(chats[room])
  } else {
    res.status(404).send({ error: 'Room not found' })
  }
})

app.post('/rooms/:room', (req, res) => {
  const room = req.params.room
  if (chats[room]) {
    if (!req.body || !req.body.name) {
      res.status(400).send({ error: 'Missing required field: name'})
    } else if (!req.body.message) {
      res.status(400).send({ error: 'Missing required field: message'})
    } else {
      if (room.startsWith("vip")) {
        try {
          const token = req.headers.authorization && req.headers.authorization.substr(7)
          const me = token && jwt.verify(token, secret)
          if (!me) {
            res.status(401).send({ error: "Channel requires auth" })
            return
          }
        } catch (e) {
          res.status(403).send({ error: "Bad token" })
          return
        }
      }
      chats[room].push({
        name: req.body.name,
        message: req.body.message,
        date: Date.now()
      })
      if (chats[room].length > maxMessages) chats[room].shift()
      res.send(chats[room])
    }
  } else {
    res.status(404).send({ error: 'Room not found' })
  }
})

app.post('/login', (req, res) => {
  if (!req.body || !req.body.username) {
    res.status(400).send({ error: 'Missing required field: username'})
  } else if (!req.body.password) {
    res.status(400).send({ error: 'Missing required field: password'})
  } else if (req.body.username !== req.body.password) {
    res.status(401).send({ error: 'Wrong password. Hint: same as username'})
  } else {
    const token = jwt.sign({ username: req.body.username }, secret)
    res.send({ token, username: req.body.username })
  }
})

app.listen(port, () => {
  console.log(`Chat API is available at: http://localhost:${port}`)
})
