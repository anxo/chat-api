# chat-api

A demo API implementing a tiny chat server

## Endpoints

The following endpoints are implemented:

- `GET /rooms`: lists the existing rooms
- `GET /rooms/:id`: lists the last messages at the given room
- `POST /rooms/:id`: sends a message to the room (required fields: `name`, `message`)
